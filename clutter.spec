Name:           clutter
Version:        1.26.4
Release:        2
Summary:        Clutter can create fast, visually rich graphical user interfaces.

License:        LGPL-2.0-or-later
URL:            https://gitlab.gnome.org/Archive/clutter
Source0:        https://download.gnome.org/sources/clutter/1.26/%{name}-%{version}.tar.xz

BuildRequires: gcc make
BuildRequires: pkgconfig(atk) >= 2.5.3
BuildRequires: pkgconfig(cairo-gobject) >= 1.14.0
BuildRequires: pkgconfig(cogl-1.0) >= 1.21.2
BuildRequires: pkgconfig(cogl-pango-1.0)
BuildRequires: pkgconfig(cogl-path-1.0)
BuildRequires: pkgconfig(egl)
BuildRequires: pkgconfig(gdk-3.0) >= 3.22.6
BuildRequires: pkgconfig(gdk-pixbuf-2.0)
BuildRequires: pkgconfig(gio-2.0) >= 2.53.4
BuildRequires: pkgconfig(gobject-2.0) >= 2.53.4
BuildRequires: pkgconfig(gobject-introspection-1.0) >= 0.9.5
BuildRequires: pkgconfig(gudev-1.0)
BuildRequires: pkgconfig(json-glib-1.0) >= 0.12.0
BuildRequires: pkgconfig(libinput) >= 0.19.0
BuildRequires: pkgconfig(libudev) >= 136
BuildRequires: pkgconfig(pangocairo) >= 1.30
BuildRequires: pkgconfig(wayland-client)
BuildRequires: pkgconfig(wayland-cursor)
BuildRequires: pkgconfig(wayland-server)
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(xcomposite) >= 0.4
BuildRequires: pkgconfig(xdamage)
BuildRequires: pkgconfig(xext)
BuildRequires: pkgconfig(xi)
BuildRequires: pkgconfig(xkbcommon)
BuildRequires: mesa-libEGL-devel
BuildRequires: gtk-doc

Requires:       cairo cogl libinput glib2 gobject-introspection mesa-dri-drivers
Requires:       json-glib

%description
Clutter is a toolkit for creating compelling, dynamic, and portable
graphical user interfaces. Clutter is free software, developed by
the GNOME community.

%package        devel
Summary:        The development environment for clutter
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-tests = %{version}-%{release}
Obsoletes:      %{name}-tests < %{version}-%{release}

%description    devel
The %{name}-devel package contains the header files and libraries for
building a extension library.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-doc = %{version}-%{release}
Obsoletes:      %{name}-doc < %{version}-%{release}

%description    help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --enable-xinput --enable-gdk-backend --enable-installed-tests \
           --enable-egl-backend --enable-evdev-input --enable-wayland-backend \
           --enable-wayland-compositor
%make_build

%install
%make_install
%delete_la

%find_lang clutter-1.0

%files -f clutter-1.0.lang
%license COPYING
%{_libdir}/libclutter-1.0.so.*
%{_libdir}/libclutter-glx-1.0.so.0
%{_libdir}/girepository-1.0/

%files devel
%{_datadir}/gir-1.0/
%{_datadir}/clutter-1.0/valgrind/clutter.supp
%{_libdir}/*.so
%{_libdir}/pkgconfig/
%{_includedir}/*
%{_datadir}/installed-tests/
%{_libexecdir}/installed-tests/clutter

%files help
%doc README.md NEWS
%{_datadir}/gtk-doc/html/clutter

%changelog
* Tue Sep 24 2024 Funda Wang <fundawang@yeah.net> - 1.26.4-2
- cleanup spec

* Mon Feb 1 2021 yanglu <yanglu60@huawei.com> - 1.26.4-1
- update to 1.26.4

* Wed Sep 9 2020 hanhui <hanhui15@huawei.com> - 1.26.2-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source url

* Sat Aug 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.26.2-9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add requires

* Thu Aug 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.26.2-8
- Package Init

